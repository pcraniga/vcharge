﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using VCharge.Models;

namespace VCharge.Repositories
{
    public class MeterReadingsRepository : IMeterReadingsRepository
    {
        public IEnumerable<MeterReading> GetMeterReadings(string filePath)
        {
            // deserialize JSON directly from a file
            using (StreamReader file = File.OpenText(filePath))
            {
                JsonSerializer serializer = new JsonSerializer();
                var readings = (List<MeterReading>)serializer.Deserialize(file, typeof(List<MeterReading>));
                return readings;
            }
        }

    }
}
