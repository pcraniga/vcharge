﻿using System;
using System.Collections.Generic;
using System.Linq;
using VCharge.Models;

namespace VCharge.Services
{
    public class MeterReadingAggregationService : IMeterReadingAggregationService
    {
        public IEnumerable<MonthlySummary> GetMonthlyData(IEnumerable<MeterReading> dayReadings)
        {
            var query =
                from s in dayReadings
                group s by new { date = new DateTime(s.ReadingDate.Year, s.ReadingDate.Month, 1) } into g
                select new MonthlySummary
                {
                    ReadingDate = g.Key.date,
                    TotalEnergy = g.Sum(x => x.Energy),
                    TotalCost = g.Sum(x => x.Energy) * g.First().Cost
                };

            return query.Count() > 0 ? query : new List<MonthlySummary>();
        }

        public IEnumerable<MonthlySummary> FilterMonthlyDataByDate(IEnumerable<MonthlySummary> monthlyData, DateTime startDate, DateTime endDate)
        {
            if (startDate < endDate) 
            {
                monthlyData = from d in monthlyData
                              where d.ReadingDate >= startDate && d.ReadingDate <= endDate
                              select d;
            }

            return monthlyData;
        }
    }
}
