﻿using System;
using System.Collections.Generic;
using VCharge.Models;

namespace VCharge.Services
{
    public interface IMeterReadingAggregationService
    {
        IEnumerable<MonthlySummary> GetMonthlyData(IEnumerable<MeterReading> dayReadings);
        IEnumerable<MonthlySummary> FilterMonthlyDataByDate(IEnumerable<MonthlySummary> monthlyData, DateTime startDate, DateTime endDate);
    }
}
