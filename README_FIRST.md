Front-end: React, Redux, Redux-Thunk, D3, Bootstrap, Webpack
Back-end: ASP.NET Core

Known Defects:
X and Y axis lines do not render
Ability to select date range where data is not available

Assumptions:
The interfaces that were provided in the original .NET solution cannot be changed.

Improvement Areas:
Implement logging, exception handling, and caching on the client and server side.
Add unit and automation tests to client.

Build and Run Instructions:
Server: From Visual Studio, open the VCharge solution, build, and then run the startup project, which should be VCharge.Web.

Client: From the VCharge.Client directory, run 'npm install' to install dependencies. Then, run 'npm start' to start the Express server.

From the web browser, navigate to http://localhost:3000/