import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/app';
import HomePage from './components/home/home-page';
import ConsumptionPage from './components/consumption/consumption-page';

export default (
  <Route path="/" component={App}>
    <IndexRoute component={HomePage} />
    <Route path="consumption" component={ConsumptionPage} />
  </Route>
);


