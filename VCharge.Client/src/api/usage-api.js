class UsageApi {

  static getUsageByDateRange({ startDate, endDate }) {
    let url = new URL("http://localhost:62252/api/usage");
    if (startDate && endDate) {
      let params = { startDate, endDate };
      Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
    }

    return fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(response => response.json());
  }

}

export default UsageApi;
