import React, { PropTypes } from 'react';
import BarChart from '../common/bar-chart';
import ButtonGroup from '../common/button-group';
import DateRangeSelector from '../common/date-range-selector';
import Dropdown from '../common/dropdown';
import UsageSummary from './usage-summary';
import toastr from 'toastr';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as consumptionActions from '../../actions/consumption-actions';

class ConsumptionPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.changeViewMode = this.changeViewMode.bind(this);
    this.dateRangeChanged = this.dateRangeChanged.bind(this);

    this.viewModes = [
      { id: 0, text: "Year", isSelected: true },
      { id: 1, text: "Month", isSelected: false },
      { id: 2, text: "Week", isSelected: false },
      { id: 3, text: "Day", isSelected: false }
    ];

    let startDate = new Date();
    startDate.setMonth(startDate.getMonth() - 12);

    this.state = {
      viewMode: 'year',
      startDate: startDate,
      endDate: new Date()
    };
  }

  changeViewMode(event) {
    let viewMode = event.target.value;
    if (viewMode !== 'year') {
      let error = `${viewMode} view mode not implemented.`;
      toastr.error(error);
      return;
    }

    this.setState({ viewMode });
  }

  dateRangeChanged(event) {
    let direction = event.target.value;

    if (this.state.viewMode === 'year') {
      let startDate = new Date(this.state.startDate);
      let endDate = new Date(this.state.endDate);

      if (direction === 'back') {
        startDate.setMonth(startDate.getMonth() - 12);
        endDate.setMonth(endDate.getMonth() - 12);
      } else {
        startDate.setMonth(startDate.getMonth() + 12);
        endDate.setMonth(endDate.getMonth() + 12);
      }
      this.setState({ startDate, endDate });
      this.props.actions.loadUsage(startDate, endDate);
    }
  }

  getDateRangeTitle(startDate, endDate) {
    let start = new Date(startDate);
    let end = new Date(endDate);

    return `${start.getMonthText()} ${start.getFullYear()} - ${end.getMonthText()} ${end.getFullYear()}`;
  }

  render() {
    let { usage } = this.props;
    let avgEnergy = '-';
    let avgCost = '-';
    let title = 'Invalid Date Range';

    if (usage.length > 0) {
      title = this.getDateRangeTitle(usage[0].readingDate, usage[usage.length - 1].readingDate);
      avgEnergy = ((usage.map(c => c.totalEnergy).reduce((a, b) => a + b, 0)) / usage.length).toFixed(2);
      avgCost = ((usage.map(c => c.totalCost).reduce((a, b) => a + b, 0)) / usage.length).toFixed(2);
    }

    return (
      <div id="consumption-page">

        <div className="row">
          <div className="time-period-selection-container">
            <ButtonGroup buttons={this.viewModes} onClick={this.changeViewMode} />
          </div>
        </div>

        <div className="row">
          <div className="col-md-5">
            <DateRangeSelector title={title} onClick={this.dateRangeChanged} />
          </div>

          <div className="col-md-3">
            <UsageSummary avgEnergy={avgEnergy} avgCost={avgCost} />
          </div>

          <div className="col-md-4">
            <Dropdown title="Select Comparison" />
          </div>
        </div>

        <div className="row">
          <BarChart data={usage} />
        </div>

      </div>
    );
  }
}

ConsumptionPage.propTypes = {
  actions: PropTypes.object.isRequired,
  usage: PropTypes.array.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    usage: state.usageData
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(consumptionActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ConsumptionPage);
