import React, { PropTypes } from 'react';

const UsageSummary = ({ avgEnergy, avgCost }) => {
  return (
    <span className="usage-summary">
      {avgEnergy} kWh {" | "} £{avgCost}
      <span className="glyphicon glyphicon-info-sign" />
    </span>
  );
};

UsageSummary.propTypes = {
  avgEnergy: PropTypes.string.isRequired,
  avgCost: PropTypes.string.isRequired
};

export default UsageSummary;
