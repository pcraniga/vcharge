import React, { PropTypes } from 'react';

const DateRangeSelector = ({ onClick, title }) => {
  return (
    <div className="date-range-selector row">
      <div className="col-md-2">
        <button type="button" className="btn btn-default" onClick={onClick} value="back">
          <span className="glyphicon glyphicon-menu-left" value="back"></span>
        </button>
      </div>

      <div className="col-md-8 range">
        <span>{title}</span>
      </div>

      <div className="col-md-2">
        <button type="button" className="btn btn-default" onClick={onClick} value="forward">
          <span className="glyphicon glyphicon-menu-right" value="forward"></span>
        </button>
      </div>

    </div>
  );
};

DateRangeSelector.propTypes = {
  onClick: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired
}

export default DateRangeSelector;
