import React, { PropTypes } from 'react';
import { Link, IndexLink } from 'react-router';

const Header = () => {
  return (
    <div id="header">
      <nav className="navbar navbar-default">
        <div className="container-fluid">

          <div className="navbar-header">
            <a className="navbar-brand" href="#">
              <img className="logo" alt="Ovo Energy" src={require('../../assets/images/ovo-energy-logo.png')} />
            </a>
          </div>

          <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul className="nav navbar-nav navbar-right">
              <li><IndexLink to="/" activeClassName="active">Home</IndexLink></li>
              <li><Link to="/consumption" activeClassName="active">Consumption</Link></li>
            </ul>
          </div>

        </div>
      </nav>
    </div>
  );
};

export default Header;
