import React, { PropTypes } from 'react';

const ButtonGroup = ({ buttons, onClick }) => {
  return (
    <div className="btn-group" role="group">
      {buttons.map(button => {
        let btnClasses = 'btn btn-default';
        if (button.isSelected) {
          btnClasses += ' active';
        }
        return <button key={button.id} type="button" onClick={onClick} value={button.text.toLowerCase()} className={btnClasses}>{button.text}</button>;
      })}
    </div>
  );
};

ButtonGroup.propTypes = {
  buttons: PropTypes.array.isRequired,
  onClick: PropTypes.func.isRequired
};

export default ButtonGroup;
