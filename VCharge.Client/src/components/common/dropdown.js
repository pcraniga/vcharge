import React, { PropTypes } from 'react';

const Dropdown = ({ title }) => {
  return (
    <div className="dropdown">
      <button className="btn btn-default dropdown-toggle" type="button"
        id="comparisonDropDownMenu" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="true">{title} <span className="caret"></span>
      </button>
    </div>
  );
};

Dropdown.propTypes = {
  title: PropTypes.string.isRequired
};

export default Dropdown;
