import React, { PropTypes } from 'react';
import * as d3 from "d3";
import Axis from './axis';
import Grid from './grid';

class BarChart extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = { width: 750 };
  }

  render() {
    let { data } = this.props;

    let parseDate = d3.time.format("%m-%d-%Y").parse;

    data.forEach(function (d) {
      d.date = parseDate(d.readingDate);
    });

    let yAxis = d3.svg.axis()
      .scale(y)
      .orient('left')
      .ticks(5);

    let xAxis = d3.svg.axis()
      .scale(x)
      .orient('bottom')
      .tickValues(data.map(function (d, i) {
        if (i > 0)
          return d.date;
      }).splice(1))
      .ticks(4);

    let margin = { top: 5, right: 5, bottom: 5, left: 5 },
      w = this.state.width - (margin.left + margin.right),
      h = this.props.height - (margin.top + margin.bottom);

    let transform = 'translate(' + margin.left + ',' + margin.top + ')';

    let x = d3.scale.ordinal()
      .domain(data.map(function (d) {
        return d.readingDate;
      }))
      .rangeRoundBands([0, this.state.width], .25);

    let y = d3.scale.linear()
      .domain([0, 1500])
      .range([this.props.height, 0]);

    let rectForeground = (data).map(function (d, i) {
      return (
        <rect fill="url(#gradient)" rx="25" ry="25" key={i}
          x={x(d.readingDate)} y={y(d.totalEnergy)} className="shadow"
          height={h - y(d.totalEnergy)}
          width={x.rangeBand()} />
      );
    });

    return (
      <div className="bar-chart">
        <svg id={this.props.chartId} width={this.state.width} height={this.props.height}>
          <defs>
            <linearGradient id="gradient" gradientTransform="rotate(90)">
              <stop className="stop1" offset="0%" />
              <stop className="stop2" offset="100%" />
            </linearGradient>
          </defs>
          <g transform={transform}>
            {rectForeground}
          </g>
        </svg>
      </div>
    );
  }
}

BarChart.propTypes = {
  chartId: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  data: PropTypes.array.isRequired
};

BarChart.defaultProps = {
  width: 600,
  height: 300,
  chartId: 'v_chart'
};

export default BarChart;
