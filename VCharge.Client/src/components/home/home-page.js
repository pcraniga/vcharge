import React from 'react';
import { Link } from 'react-router';

class HomePage extends React.Component {
  render() {
    return (
      <div id="home-page">
        <div className="jumbotron">
          <h1>Welcome to My OVO</h1>
          <p>View your yearly consumption broken down by calendar month.</p>
          <Link to="consumption" className="btn btn-primary btn-lg">Get Started</Link>
        </div>
      </div>
    );
  }
}

export default HomePage;
