import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';

import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import routes from './routes';

import configureStore from './store/configure-store';
import { loadUsage } from './actions/consumption-actions';

import './styles/global.css';
import './utils/date';
import './components/common/bar-chart.css';
import './components/common/date-range-selector.css';
import './components/common/header.css';
import './components/consumption/consumption-page.css';
import './components/home/home-page.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

import '../node_modules/toastr/build/toastr.min.css';

let startDate = new Date();
startDate.setMonth(startDate.getMonth() - 12);
let endDate = new Date();

const store = configureStore();
store.dispatch(loadUsage(startDate, endDate));

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('app')
);
