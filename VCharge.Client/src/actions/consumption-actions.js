import * as types from './action-types';
import UsageApi from '../api/usage-api';
import { beginAjaxCall, ajaxCallError } from './ajax-status-actions';

export function loadUsageSuccess(usageData) {
  return { type: types.LOAD_USAGE_SUCCESS, usageData };
}

export function loadUsage(startDate, endDate) {
  return function (dispatch) {
    dispatch(beginAjaxCall());

    return UsageApi.getUsageByDateRange({ startDate: startDate.toISOString(), endDate: endDate.toISOString() }).then(ud => {
      dispatch(loadUsageSuccess(ud.reverse()));
    }).catch(error => {
      throw error;
    });
  };
}


