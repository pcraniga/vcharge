import { combineReducers } from 'redux';
import usageData from './consumption-reducer';
import ajaxCallsInProgress from './ajax-status-reducer';

const rootReducer = combineReducers({
  usageData,
  ajaxCallsInProgress
});

export default rootReducer;
