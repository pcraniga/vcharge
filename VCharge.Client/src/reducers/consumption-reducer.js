import * as types from '../actions/action-types';
import initialState from './initial-state.js';

export default function consumptionReducer(state = initialState.usageData, action) {
  switch (action.type) {
    case types.LOAD_USAGE_SUCCESS:
      return action.usageData;

    default:
      return state;
  }
}
