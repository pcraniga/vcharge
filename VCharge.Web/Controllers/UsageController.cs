﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using VCharge.Models;
using VCharge.Repositories;
using VCharge.Services;

namespace VCharge.Web.Controllers
{
    [Route("api/[controller]")]
    public class UsageController : Controller
    {
        private readonly IMeterReadingAggregationService _aggregationService;
        private readonly IMeterReadingsRepository _repository;
        private readonly IHostingEnvironment _env;

        public UsageController(
            IMeterReadingAggregationService aggregationService,
            IMeterReadingsRepository repository,
            IHostingEnvironment env)
        {
            _aggregationService = aggregationService;
            _repository = repository;
            _env = env;
        }

        // GET api/usage
        [HttpGet]
        public IEnumerable<MonthlySummary> Get(DateTime? startDate, DateTime? endDate)
        {
            var filePath = GetFilePath();
            var meterReadings = _repository.GetMeterReadings(filePath);

            var monthlyData = _aggregationService.GetMonthlyData(meterReadings);
            if (startDate.HasValue && endDate.HasValue)
            {
                return _aggregationService.FilterMonthlyDataByDate(
                    monthlyData,
                    startDate.Value,
                    endDate.Value);
            }

            return monthlyData;
        }

        // GET api/usage/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "test";
        }

        private string GetFilePath()
        {
            var separator = Path.DirectorySeparatorChar.ToString();
            return _env.ContentRootPath
                           + separator
                           + "Data"
                           + separator
                           + "MOCK_DATA.json";
        }
    }
}
