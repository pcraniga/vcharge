﻿using System;

namespace VCharge.Models
{
    public class MonthlySummary
    {
        public DateTime ReadingDate { get; set; }
        public double TotalCost { get; set; }
        public double TotalEnergy { get; set; }
    }
}
