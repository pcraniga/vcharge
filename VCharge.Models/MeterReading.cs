﻿using System;

namespace VCharge.Models
{
    public class MeterReading
    {
        public DateTime ReadingDate { get; set; }
        public double Energy { get; set; }
        public double Cost { get; set; }
    }
}
